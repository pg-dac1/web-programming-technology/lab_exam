import { Link } from "react-router-dom";

const Header = () =>{

    return(
        <div>
             <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"> <Link to={'/get-all'}> Get All </Link>   </li>
                    <li class="breadcrumb-item"><Link to={'/insert'}> Insert </Link> </li>
                    <li class="breadcrumb-item"><Link to={'/delete'}> Delete </Link> </li>                    
                </ol>
            </nav>
        </div>
    )
}

export default Header;
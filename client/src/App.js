import Signin from './pages/Signin';
import './App.css';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { useEffect } from 'react';
import Header from './Components/Header';
import Getall from './pages/Getall';
import Insert from './pages/Insert';
import DeleteCar from './pages/Delete';

function App() {

  const LandingPage = () => {
    return <Signin />
  }

  useEffect(() => { LandingPage() }, [])

  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<LandingPage />} />
          <Route path='/signin' element={<Signin />} />
          <Route path='/admin' element={<Header />}></Route>
          <Route path='/get-all' element={<Getall />}></Route>
          <Route path='/insert' element={<Insert />}></Route>
          <Route path='/delete' element={<DeleteCar />}></Route>
        </Routes>
      </BrowserRouter>
      <ToastContainer />
    </div>
  );
}

export default App;

import axios from "axios";
import { useEffect } from "react";
import Car from "../../Components/Car";
import { useState } from "react";

const Getall = () =>{

    const [result, setResult] = useState([])

    const GetCars = () =>{
        const url = `http://localhost:4000/car/get-all`

        axios.get(url).then((response)=>{
            setResult(response.data);
        });
    }

    useEffect(()=>{GetCars()},[])
    
    return (
        <div>
            {result.map( (c) => {return <Car res= {c} > </Car>  }  )}
        </div>
    )

}

export default Getall;
const { request, response } = require('express');
const express = require('express');
const db = require('../db')
const utils = require('../utils')
const router = express.Router();


router.post('/insert',(request,response)=>{
   
    const connection = db.openConnection();

    const {id,name,model,price,carColor} = request.body;

    const statement = `insert into carTB values(${id}, '${name}', '${model}',
                         ${price}, '${carColor}')`;

    connection.query(statement,(error,result)=>{
        connection.end();

        response.send(utils.createResult(error,result));
    });                       
});


router.get('/get-all',(request,response)=>{
    
    const connection  = db.openConnection();
    console.log('Hello');
    const statement = `select * from carTB`;

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResult(error,result));
    });
});

router.delete('/delete/:id',(request,response)=>{

    const connection = db.openConnection();

    const {id} = request.params;

    const statement = `delete from carTB where id = ${id}`

    connection.query(statement,(error,result)=>{
        connection.end();
        response.send(utils.createResult(error,result));
    });

});

module.exports = router;



const mysql = require("mysql")
const mysql2 = require('mysql2/promise')

module.exports.openConnection = () => {

    const connection = mysql.createConnection({
        port : 3306,

        user : 'root',

        password : 'manager',

        database : 'express'
    });

    connection.connect();

    return connection;
}

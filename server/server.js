const express =  require('express')
const app = express();
const db = require('./db')
const cors = require('cors')

const userRouter = require('./routes/user');
const carRouter = require('./routes/car')
const bodyParser = require('body-parser');

// enable CORS (Cross Origin Resource Sharing)
// needed for client to call the apis from different url
app.use(cors('*'))

// add the json parser to parse the json data sent through
// the request body
app.use(express.json())

app.use(bodyParser.json())

//use employee router
app.use('/user',userRouter)

//use car router
app.use('/car',carRouter)

app.listen(4000,()=>{
    console.log("Server started on port 4000");
})